year = int(input("Please input a year \n"))

i = 2016
while i <= 5000:
    i += 4
    if year == i:
        print(f"{year} is a leap year")
        break
if year != i:
    print(f"{year} is not a leap year")

rows = int(input("Enter number of rows \n"))
columns = int(input("Enter number of columns \n"))

col = ""
for k in range(100):
    if k == columns:
        break
    else:
        col += "*"

for x in range(100):
    if x == rows:
        break
    else:
        print(col)
